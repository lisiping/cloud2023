package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @description:
 * @Author: Lee
 * @Created: 2023/10/13 11:15
 */
@RestController
@Slf4j
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment){
        int result = paymentService.create(payment);
        log.info("新增结果{}",result);
        if (result>0) {
            return new CommonResult(200,"新增成功！");
        } else {
            return new CommonResult(500,"新增失败！");
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getOneById(@PathVariable("id") Long id){
        Payment payment = paymentService.getPaymentById(id);
        log.info("查询结果：{}",payment);
        if (payment!=null) {
            return new CommonResult(200,"查询成功",payment);
        } else {
            return new CommonResult(200,"查询失败",payment);
        }
    }
}
